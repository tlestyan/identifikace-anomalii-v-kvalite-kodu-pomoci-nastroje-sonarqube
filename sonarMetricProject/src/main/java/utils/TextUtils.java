/**
 * The MIT License (MIT)
 * Copyright (c) 2016 Tomas Lestyan
 */
package main.java.utils;

/**
 * TText constants and methods
 * @author Tomas Lestyan
 */
public class TextUtils {

	/**
	 * Constructor
	 */
	private TextUtils () {
		// do not allow to create instances
	}

	public static final String DESCRIPTION = "description";
	public static final String NAME = "name";
	public static final String METRIC_ID = "metric_id";
	public static final String TRESHOLD_PERCENTILE = "tresholdPercentile";
	public static final String VALUE = "value";

}
