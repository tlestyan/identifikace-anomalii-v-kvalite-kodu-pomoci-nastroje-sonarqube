package main.java.plugin;

import org.sonar.api.ce.measure.Measure;
import org.sonar.api.ce.measure.MeasureComputer;
import org.sonar.api.measures.CoreMetrics;

/**
 * Test mesasure computer
 * @author Tomas Lestyan
 */
@Deprecated
public class TestMeasureComputer implements MeasureComputer {

	/* (non-Javadoc)
	 * @see org.sonar.api.ce.measure.MeasureComputer#define(org.sonar.api.ce.measure.MeasureComputer.MeasureComputerDefinitionContext)
	 */
	@Override
	public MeasureComputerDefinition define(MeasureComputerDefinitionContext defContext) {
		return defContext.newDefinitionBuilder()
				.setInputMetrics(DisharmoniesMetrics.TEST.getKey(), CoreMetrics.LINES.getKey())
				.setOutputMetrics(DisharmoniesMetrics.DISHARMONY.getKey())
				.build();
	}

	/* (non-Javadoc)
	 * @see org.sonar.api.ce.measure.MeasureComputer#compute(org.sonar.api.ce.measure.MeasureComputer.MeasureComputerContext)
	 */
	@Override
	public void compute(MeasureComputerContext context) {
		Measure fileNum = context.getMeasure(CoreMetrics.LINES.getKey());
		if (fileNum != null) {
			int value = fileNum.getIntValue() * 2;
			context.addMeasure(DisharmoniesMetrics.DISHARMONY.getKey(), Integer.toString(value));
		}
	}
}
