package main.java.plugin;

import java.util.ArrayList;
import java.util.List;

import org.sonar.api.measures.CoreMetrics;
import org.sonar.api.measures.Metric;
import org.sonar.api.measures.Metrics;

/**
 * @author Tomas Lestyan
 */
@SuppressWarnings("rawtypes")
@Deprecated
public class DisharmoniesMetrics implements Metrics {

    /**
     * Test metric
     */
    public static final Metric TEST=
        new Metric.Builder(
            "test",
            "Test metric",
            Metric.ValueType.STRING)
            .setDescription("Metric for test purposes")
            .setQualitative(false)
            .setDomain(CoreMetrics.DOMAIN_GENERAL)
            .create();

   /**
    * Test disharmony metric
    */
   public static final Metric DISHARMONY=
       new Metric.Builder(
           "disharmony",
           "Disharmony",
           Metric.ValueType.STRING)
           .setDescription("Disharmony for test purposes")
           .setQualitative(false)
           .setDomain(CoreMetrics.DOMAIN_GENERAL)
           .create();

    /**
     * Constructor
     */
    public DisharmoniesMetrics() {
        super();
    }

	/* (non-Javadoc)
	 * @see org.sonar.api.measures.Metrics#getMetrics()
	 */
	@Override
	public List<Metric> getMetrics() {
    	List<Metric> metrics = new ArrayList<>();
    	metrics.add(TEST);
    	metrics.add(DISHARMONY);
        return metrics;
    }
}
